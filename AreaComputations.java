public class AreaComputations{

//Static Method to calculate the area of a square
public static int areaSquare(int lengthOneSide){
		int area = lengthOneSide * lengthOneSide;
	    return area;
		
	}
	
	//Instance Method to calculate the area of a rectangle
	 public int areaRectangle(int length, int width){
		    int area = length * width;
		    return area;

	 }

}