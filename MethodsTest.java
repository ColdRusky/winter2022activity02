public class MethodsTest{

public static void main(String[] args){
	
	SecondClass sc = new SecondClass();
	System.out.println(SecondClass.addOne(50));
		System.out.println(sc.addTwo(50));
}

public static void methodNoInputNoReturn(){
	
	System.out.println("Im in a method that takes no input and returns nothing!");
	int x = 50;
		System.out.println(x);
	
	}
	
	public static void methodOneInputNoReturn(int a){
		
		System.out.println("inside method one input no return! "+ a);
	}
	
	public static void methodTwoInputNoReturn(int b,double c){
		
		System.out.println("inside method two input no return! "+ b +" and "+ c);
	}
	
	public static int methodNoInputReturnsInt(){
		
		System.out.println("inside method no input returns int! ");
		return 6;
	}
	
	public static double sumSquareRoot(int a, int b){
			double result = a + b;
			result = Math.sqrt(result);
			return result;
		}
	
	
}