import java.util.Scanner;

public class PartThree{

public static void main(String[] args) {
		
		//Making an input for the user
		Scanner input = new Scanner(System.in);
		
		//Asking the user to input the length of 1 side of the square
		System.out.println("Enter the value for the length of one side of the square: ");
	    int lengthOneSide = input.nextInt();
	    
	    //Displaying the area of the square to the user
	    System.out.println("Area of the square: "+ AreaComputations.areaSquare(lengthOneSide));
	    
	    //Initializing the AreaComputations class to call the instance method(areaRectangle) in the main method
	    AreaComputations computing = new AreaComputations();
	    
	    //Asking the user to input the length of the rectangle
	  System.out.println("Enter the value for the length of the rectangle: ");
	  int length = input.nextInt();
	  
	  //Asking the user to input the width of the rectangle
	  System.out.println("Enter the value for the width of the rectangle: ");
	  int width = input.nextInt();
	  
	  //Calculating the area of the rectangle with the users values entered and with the areaRectangle method from the AreaComputations class
	    int area = computing.areaRectangle(length, width);
	    
	    //Displaying the area of the rectangle to the user
	    System.out.println("Area of the rectangle: "+ area);
	
	}

}